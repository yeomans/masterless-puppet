#! /bin/bash

set -e
set -u
set -o pipefail


if ! id -u >> /dev/null; then
    echo "This script must be executed as root."
    exit 1
fi

# parse named arguments
REPO_DIR=$(pwd)
DO_PULL=true

while true; do
    case "${1:-unset}" in 
        --repo-dir)
            shift
            REPO_DIR="$1"
            shift
            ;;
        --no-pull)
            DO_PULL=false
            shift
            ;;
        *)
        break
    esac
done

# Whether REPO_DIR was set by command-line or implicitly by pwd, we make sure that it lives
# in a git repository, and then cd to the root directory of that repository. If not, the git
# command should fail and the script abort. This bit of bash has not been tested with symlinks.

REPO_DIR=$(cd "$REPO_DIR"; git rev-parse --show-toplevel)
cd "$REPO_DIR"
$DO_PULL && git pull origin $(git symbolic-ref --short HEAD)

# in version 5, puppet config print appears to return configuration values sorted by name instead of using the order of parameters. 
read PUPPET_CODEDIR PUPPET_CONFDIR <<< $(echo $(puppet config print codedir confdir | awk -F '='   '{gsub(/ /, "", $2); print $2}'))

# puppet config print basemodule path returns a list of module locations.  We want to put
# modules in the system location instead of the puppet code repository, as this would 
# conflict with git.
PUPPET_MODULEDIR="/opt/puppetlabs/puppet/modules"

puppet-modulator.py --repodir "$REPO_DIR" --targetdir "$PUPPET_MODULEDIR"
puppet apply --confdir "$PUPPET_CONFDIR" "$PUPPET_CODEDIR/environments/production/manifests/init.pp"
