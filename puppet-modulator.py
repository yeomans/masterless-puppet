#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import argparse
import os
import json
import subprocess
import socket
import inspect


def get_git_branch(repo_dir):
    assert repo_dir
    output = subprocess.check_output(['/usr/bin/git', 'rev-parse', '--abbrev-ref', 'HEAD'], stderr=subprocess.STDOUT, cwd=repo_dir)
    return output.decode('utf-8').strip()


def get_puppet_config(*keys):
    """ Retrieves the puppet configuration items as specified in keys, parses output because
    puppet --render-as may not work, and returns a dictionary,"""
    
    output = subprocess.check_output(['/opt/puppetlabs/bin/puppet', 'config', 'print'] + list(keys), stderr=subprocess.PIPE)
    item_list = [x for x in output.decode('utf-8').split('\n') if x]
    items = {y[0].strip(): y[1].strip() for y in [x.split('=') for x in item_list]}
    return items


def get_modules(repo_dir):
    """Returns a dictionary of module objects as retrieved from puppet hiera configuration."""

    # Puppet writes warnings to stderr, and it appears that --disable_warnings deprecations
    # does not work as of 4.10.1.  So we don't direct stderr to STDOUT.
    output = subprocess.check_output(['/opt/puppetlabs/bin/puppet', 'lookup', 'modules', '--merge', 'hash', '--render-as', 'json'], cwd=repo_dir)
    return json.loads(output.decode('utf-8'))

def update_module_from_forge(name, *, force=False, ignore_dependencies=False, target_dir=None, version=None):
    args = ['/opt/puppetlabs/bin/puppet', 'module', 'install']
    if force:
        args.extend(['--force'])
    if ignore_dependencies:
        args.extend(['--ignore-dependencies'])
    if target_dir:
        args.extend(['--target-dir', target_dir])
    if version:
        args.extend(['--version', version])
    args.extend([name])

    output = subprocess.check_output(args, stderr=subprocess.PIPE)
    print(output.decode('utf-8'))


def update_module_from_git(name, *, target_dir, remote, repo_name=None, branch=None, tag=None, commit=None):
    if repo_name is None:
        repo_name = name

    repo_path = os.path.join(target_dir, repo_name)
    version = [x for x in [tag, branch, commit, 'master'] if x][0]

    if not os.path.exists(repo_path):
        print('Cloning repository %s.' % repo_name)
        subprocess.check_output(['/usr/bin/git', 'clone', '--quiet', '--branch', version, remote, repo_path], stderr=subprocess.PIPE)
    else:
        subprocess.check_output(['/usr/bin/git', 'fetch', 'origin', '--prune'], stderr=subprocess.PIPE, cwd=repo_path)
        # check to see whether branch has been changed.
        current_branch = get_git_branch(repo_path)
        if current_branch != version:
            print('Module branch has been changed from %s to %s.  Checking out branch %s.' % (current_branch, version, version))
            subprocess.check_output(['/usr/bin/git', 'checkout', version], stderr=subprocess.PIPE, cwd=repo_path)

        # git symbolic-ref --short -q HEAD returns 0 and prints branch name if on a branch.
        # if repo in detached state (e.g. tag or commit checked out), returns 1 and
        # prints nothing.
        if subprocess.call(['/usr/bin/git', 'symbolic-ref', '--quiet', 'HEAD'], stdout=open(os.devnull, 'wb'), cwd=repo_path) == 0:
            local_commit = subprocess.check_output(['/usr/bin/git', 'rev-parse', 'HEAD'], stderr=subprocess.PIPE, cwd=repo_path).decode('utf-8')
            remote_commit = subprocess.check_output(['/usr/bin/git', 'rev-parse', '@{u}'], stderr=subprocess.PIPE, cwd=repo_path).decode('utf-8')
            if local_commit != remote_commit:
                print('Merging changes.')
                output = subprocess.check_output(['/usr/bin/git', 'merge', '--ff-only'], stderr=subprocess.PIPE, cwd=repo_path)
                print(output.decode('utf-8'))

def update_module(name, **kwargs):
    provider = kwargs.get('provider', 'puppet-forge')
    if provider == 'puppet-forge':
        func = update_module_from_forge
    elif provider == 'git':
        func = update_module_from_git
    else:
        raise Exception('Unsupported provider "%s".' % provider)

    module_args = {}
    for key in inspect.getfullargspec(func).kwonlyargs:
        if key in kwargs:
            module_args[key] = kwargs[key]
    print('Updating module %s.' % name)
    func(name, **module_args)


def main(argv=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('--repodir', required=True, help='Path to git repository containing puppet configuration for server.')
    parser.add_argument('--targetdir', required=False, help='Location for puppet module installation. Can be overridden by a module. Default location is the first path in puppet modulepath.')

    args = parser.parse_args()
    modules = get_modules(args.repodir)
    try:
        for name in modules:
            module_options = modules[name]
            if args.targetdir and 'target_dir' not in module_options:
                module_options['target_dir'] = args.targetdir
            update_module(name, **module_options)
    except subprocess.CalledProcessError as e:
        print(e.cmd, e.returncode, e.output)
        return 1


if __name__ == "__main__":
    sys.exit(main())
