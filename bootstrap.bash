#! /bin/bash


# --hostaddr: IP address of server.  Required.
# --hostname: hostname of server.  Not required, but probably needed if hiera.yaml uses
# it as a selector.
# --ssh-port: SSH port on remote machine.  When configuring a VirtualBox, the port is 
#     usually 2222.  Default value is 22.
# --install-git-server-repo: If this flag is passed, a bare repo will be installed in 
# /var/lib/git and set to be the origin repository of the repository uploaded by this script.
# $1: path to git repository containing the puppet configuration to be installed.
set -e
set -u
set -o pipefail

# initialize variables with named argument defaults.
PORT=22
INSTALL_BARE_REPO=false
# parse named arguments
while true; do
    case "${1:-unset}" in  
        --hostaddr)
            shift
            HOSTADDR="$1"
            shift
            ;;
        --hostname)
            shift
            HOSTNAME="$1"
            shift
            ;;
        --ssh-port)
            shift
            PORT="$1"
            shift
            ;;
        --install-git-server-repo)
            INSTALL_BARE_REPO=true
            shift
            ;;
        *)
        break
    esac
done

# what's left should be the positional arguments.
PUPPET_REPO="$1"

if [[ -z "$PUPPET_REPO" ]]; then
    >&2 echo "No path to git repository supplied." 
    exit 1
fi

pushd "$PUPPET_REPO" > /dev/null
CURRENT_BRANCH=$(git symbolic-ref --short --quiet HEAD)
popd > /dev/null
# get the path to the local repository containing this script.  I assume that this script 
# lives in the top level of the repository.
SERVER_WORKING_REPO="/root/$(basename "$PUPPET_REPO")"

echo "You are about to configure the server at $HOSTADDR with hostname $HOSTNAME using branch $CURRENT_BRANCH of the repository $PUPPET_REPO."
echo "Do you wish to proceed?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) exit;;
    esac
done



GIT_HOME="/var/lib/git"
REPO_NAME=$(basename "$SERVER_WORKING_REPO")
SERVER_ORIGIN_REPO="$GIT_HOME/$REPO_NAME.git"
ROOT_KNOWN_HOSTS="/root/.ssh/known_hosts"
PUPPET_BINDIR=/opt/puppetlabs/bin
PUPPET_MODULEDIR=/opt/puppetlabs/puppet/modules

# first, ensure that rsync is installed on machine.  Recent linode VMs don't come with it
# pre-installed.
echo "Ensuring that rsync is installed on target machine."
ssh -p "$PORT" root@"$HOSTADDR" apt-get install --quiet --quiet rsync

echo "Copying puppet scripts to $HOSTADDR."
pushd "$(dirname "$0")" > /dev/null
rsync --rsh "ssh -p $PORT" run_puppet.bash puppet-modulator.py root@"$HOSTADDR":/usr/local/bin/
popd > /dev/null

# should I check to make sure that the local repo has no changes?
# check to make certain that there is a remote server repo?

echo "Copying repository to $HOSTADDR."
if ! rsync --verbose --archive --compress --delete --rsh "ssh -p $PORT" "$PUPPET_REPO" root@"$HOSTADDR":~ ; then
    echo "Copy failed." >&2
    exit 1
fi

echo "Performing initial setup."
ssh -p "$PORT" -T root@"$HOSTADDR" <<EOF
#! /bin/bash

set -e
set -u
set -o pipefail

echo "Setting hostname to $HOSTNAME."
echo "$HOSTNAME" > /etc/hostname
hostname -F /etc/hostname

echo "Fixing ownership of repository."
chown root:root "$SERVER_WORKING_REPO"

echo 'Setting permissions for masterless-puppet files.'
chown root:root /usr/local/bin/puppet-modulator.py /usr/local/bin/run_puppet.bash
chmod u+x /usr/local/bin/puppet-modulator.py /usr/local/bin/run_puppet.bash

echo "Configuring puppet apt repository."
# repository for Puppet 5.
PUPPET_REPO_DEB="puppet5-release-\$(lsb_release --codename --short).deb"
wget https://apt.puppetlabs.com/\$PUPPET_REPO_DEB
dpkg -i \$PUPPET_REPO_DEB
rm \$PUPPET_REPO_DEB

export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get --yes upgrade

echo "Installing a few base packages."
apt-get install --yes git python3

echo "Installing puppet."
apt-get install --yes puppet-agent

echo "Disabling puppet and mcollective."
systemctl stop mcollective
systemctl disable mcollective
systemctl stop puppet
systemctl disable puppet

# Puppet now installs its executables in PUPPET_BINDIR, and doesn't do a good job of
# updating PATH.  So we create some symlinks.

for PUPPET_BIN in \$(ls "$PUPPET_BINDIR")
    do
    echo "Linking $PUPPET_BINDIR/\$PUPPET_BIN" to /usr/local/bin/"\$PUPPET_BIN".
    ln -sf "$PUPPET_BINDIR/\$PUPPET_BIN" /usr/local/bin/"\$PUPPET_BIN"
    done

PUPPET_CONFDIR="\$($PUPPET_BINDIR/puppet config print confdir)"

if [[ -d \$PUPPET_CONFDIR ]] && [[ ! -e \$PUPPET_CONFDIR.default ]]; then
    echo "Moving default puppet configuration \$PUPPET_CONFDIR to \$PUPPET_CONFDIR.default."
    mv "\$PUPPET_CONFDIR" "\$PUPPET_CONFDIR".default
fi

echo "Linking puppet_conf directory to \$PUPPET_CONFDIR."
rm -f "\$PUPPET_CONFDIR"
ln -sf "$SERVER_WORKING_REPO"/puppet_conf "\$PUPPET_CONFDIR"

PUPPET_CODEDIR="\$($PUPPET_BINDIR/puppet config print codedir)"

if [[ -d \$PUPPET_CODEDIR ]] && [[ ! -e \$PUPPET_CODEDIR.default ]]; then
    echo "Moving default puppet code \$PUPPET_CODEDIR to \$PUPPET_CODEDIR.default."
    mv "\$PUPPET_CODEDIR" "\$PUPPET_CODEDIR".default
fi
echo "Linking puppet_code directory to \$PUPPET_CODEDIR."
rm -f "\$PUPPET_CODEDIR"
ln -sf "$SERVER_WORKING_REPO"/puppet_code "\$PUPPET_CODEDIR"

echo "Linking facter directory to /etc/puppetlabs/facter/facts.d."
mkdir -p /etc/puppetlabs/facter
ln -sf "$SERVER_WORKING_REPO"/facter /etc/puppetlabs/facter/facts.d

# We install ssh keys for root so that puppet can get modules, etc. from 
# private repositories.  The puppet server configuration also manages root keys; the best 
# solution to the bootstrap problem is to read the keys from hiera and install now.
# We also install known_hosts from hiera so that git and puppet can connect over ssh to remote
# repositories.
echo "Installing ssh keys for root."
pushd "$SERVER_WORKING_REPO" > /dev/null
mkdir -m 0700 -p /root/.ssh

puppet lookup server_base::root::private_key --merge hash --render-as s > /root/.ssh/id_rsa
puppet lookup server_base::root::public_key --merge hash --render-as s > /root/.ssh/id_rsa.pub

echo "Initializing root known hosts."
SSH_KNOWN_HOSTS=\$(puppet lookup server_base::root::ssh_known_hosts --merge hash --render-as json)
echo "\$SSH_KNOWN_HOSTS" | python -c 'import json, sys; print("\n".join([" ".join([key, value["type"], value["key"]]) for key, value in json.load(sys.stdin).items()]))' > "$ROOT_KNOWN_HOSTS"
chmod 0600 /root/.ssh/*
popd

if $INSTALL_BARE_REPO ; then
    echo "Removing existing remotes from working repository."
    pushd "$SERVER_WORKING_REPO" >> /dev/null
    git remote | xargs -I {} git remote rm {}
    popd >> /dev/null
    
    if ! getent group git >> /dev/null; then
        echo "Creating group 'git'."
        groupadd --system git
    fi

    if ! getent passwd git >> /dev/null ; then
        echo "Creating user 'git'."
        useradd --home-dir "$GIT_HOME" --system --shell /usr/bin/git-shell --gid git git
    fi

    echo "Creating git home directory if needed."
    mkdir -p "$GIT_HOME"
    
    echo "Setting up ssh access for root."
    mkdir -p "$GIT_HOME/.ssh"
    chmod 700 "$GIT_HOME/.ssh"
    pushd "$SERVER_WORKING_REPO" > /dev/null
    puppet lookup server_base::root::public_key --merge hash --render-as s > "$GIT_HOME"/.ssh/authorized_keys
    popd > /dev/null
    chmod 600 "$GIT_HOME"/.ssh/*
    
    echo "Creating bare origin repository."
    rm -rf "$SERVER_ORIGIN_REPO"
    git clone --bare file://"$SERVER_WORKING_REPO" "$SERVER_ORIGIN_REPO"
        
    echo "Removing existing remotes from origin repository."
    pushd "$SERVER_ORIGIN_REPO" >> /dev/null
    git remote | xargs -I {} git remote rm {}
    popd >> /var/null

    echo "Creating /root/.ssh if needed."
    mkdir -p /root/.ssh
    chmod 700 /root/.ssh
    echo "Configuring $ROOT_KNOWN_HOSTS for working repository."
    if [[ -x "$ROOT_KNOWN_HOSTS" ]]; then
        ssh-keygen -R localhost -f "$ROOT_KNOWN_HOSTS"
        ssh-keygen -R 127.0.0.1 -f "$ROOT_KNOWN_HOSTS" 
        ssh-keygen -R localhost,127.0.0.1 -f "$ROOT_KNOWN_HOSTS"
    fi
    (ssh-keyscan -H localhost,127.0.0.1; ssh-keyscan -H 127.0.0.1; ssh-keyscan -H localhost;) >> "$ROOT_KNOWN_HOSTS"

    echo "Setting ownership of $GIT_HOME."
    chown -R git:git "$GIT_HOME"
    
    echo "Removing uploaded repository copy."
    rm -r "$SERVER_WORKING_REPO"
    echo "Cloning origin to working repository."
    pushd /root >> /var/null
    git clone --branch "$CURRENT_BRANCH" git@localhost:"$(basename "$SERVER_ORIGIN_REPO")" "$SERVER_WORKING_REPO"
    popd >> /var/null
fi

echo "Running puppet."
run_puppet.bash --repo-dir "$SERVER_WORKING_REPO" --target-dir "$PUPPET_MODULEDIR"


echo "Bootstrappery is complete."
EOF
